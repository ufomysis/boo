package config

type Config struct {
	DB *DBConfig
}

type DBConfig struct {
	Dialect  string
	Host     string
	Port     int
	Username string
	Password string
	Name     string
	Charset  string
}

func GetConfig() *Config {
	return &Config{
		DB: &DBConfig{
			Dialect:  "mysql",
			Host: "database-1.cmda436ecq4i.ap-southeast-1.rds.amazonaws.com",
			Port: 3306,
			Username: "admin",
			Password: "DBqmesis606!!",
			Name:     "todoapp",
			Charset:  "utf8",
		},
	}
}
