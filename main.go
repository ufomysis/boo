package main

import (
	"http://ec2-13-213-16-111.ap-southeast-1.compute.amazonaws.com/boosas/boo.git/app"
	"http://ec2-13-213-16-111.ap-southeast-1.compute.amazonaws.com/boosas/boo.git/config"
)

func main() {
	config := config.GetConfig()

	app := &app.App{}
	app.Initialize(config)
	app.Run(":3000")
}
